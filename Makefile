TARGET_DIR := /usr/share/plymouth/themes/dark-gathering
SHELL := /bin/bash

install:
	mkdir -p $(TARGET_DIR)
	cp -r images *.{plymouth,script} $(TARGET_DIR)

resize:
	cd images; \
	for i in {1..100}; \
		do ffmpeg -i progress-$$i.png -vf scale=160:160 -y lowprogress-$$i.png; \
		mv -f lowprogress-$$i.png progress-$$i.png; \
	done
