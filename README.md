# Dark Gathering plymouth theme
Plymouth theme by using script module

## Step to generate the animated file
1. Open blender in video editing mode and render animation by pressing `ctrl+f12`
2. then resize the image by running `make resize`
3. Export the endscreen in gimp and name it `endscreen.png`
4. The generated image should be in the [images folder](./images/)
5. Install it by running `sudo make install`
6. Set the default theme by executing `sudo plymouth-set-default-theme -R dark-gathering`

### License
MIT License
